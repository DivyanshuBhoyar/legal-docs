import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { NavbarComponent } from '../shared-components/navbar/navbar.component';
import { SidebarComponent } from '../shared-components/sidebar/sidebar.component';
import { LegalDocsFormComponent } from '../legal-docs-form/legal-docs-form.component';

import { CalendarModule } from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { StepperComponent } from '../legal-docs-form/stepper/stepper.component';


@NgModule({
  declarations: [
    LegalDocsFormComponent,
    HomepageComponent, NavbarComponent, SidebarComponent, StepperComponent],
  imports: [
    CommonModule,
    CalendarModule,
    HomeRoutingModule,
    FormsModule
  ]
})
export class HomeModule { }
