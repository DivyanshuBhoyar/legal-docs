import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalDocsFormComponent } from './legal-docs-form.component';

describe('LegalDocsFormComponent', () => {
  let component: LegalDocsFormComponent;
  let fixture: ComponentFixture<LegalDocsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegalDocsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalDocsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
