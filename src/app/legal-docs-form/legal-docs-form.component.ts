import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-legal-docs-form',
  templateUrl: './legal-docs-form.component.html',
  styleUrls: ['./legal-docs-form.component.scss']
})
export class LegalDocsFormComponent implements OnInit {
  dateValue: Date
  activeSection: number
  constructor() { }

  onButtonClick() {
    if (this.activeSection === 3) this.submitForm()
    this.activeSection = this.activeSection + 1
  }

  submitForm() {
    console.log("here");

  }
  ngOnInit(): void {
    this.activeSection = 1
  }

}
