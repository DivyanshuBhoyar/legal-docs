import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

  @Input() sectionSelected: number = 1
  @Output() onSectionSelected = new EventEmitter()

  constructor() { }

  onClick(step: number) {
    this.sectionSelected = step
    this.onSectionSelected.emit(step)
  }

  ngOnInit(): void {
  }

}
