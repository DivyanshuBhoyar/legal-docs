import { Injectable } from '@angular/core';
import NewUser from '../models/NewUser';
import LoginUser from '../models/LoginUser';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import OkRegistration from '../models/OkRegistration';
import OkEmail from '../models/OkEmail'; import OKLogin from '../models/OkLogin';
'../models/OkEmail';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private api = "https://ac7a-117-233-95-156.ngrok.io"
  private registerURL = this.api + "/register/";
  private loginURL = this.api + '/login/';
  private checkEmailURL = this.api + "/otp/"

  public loginUser: LoginUser = {
    email: '',
    otp: '',
  }

  constructor(private http: HttpClient) { }

  checkEmailExists(email: any): Observable<OkEmail> {
    return this.http.post<OkEmail>(this.checkEmailURL, email, httpOptions)

  }

  loginWithEmailOTP(): Observable<OKLogin> {
    return this.http.post<OKLogin>(this.loginURL, this.loginUser, httpOptions)
  }

  registerUser(user: NewUser): Observable<OkRegistration> {
    return this.http.post<OkRegistration>(this.registerURL, user, httpOptions)
  }
}
