export default interface OKLogin {
   message: string,
   email: string,
   mobile: string,
   emp_code: string,
   success: boolean,
   loginId: number,
   auth_token: string,
   firstname: string,
   lastname: string,
   isActive: boolean,
   hod: string | any,
   department: string,
   authority: any,
   role: any
}