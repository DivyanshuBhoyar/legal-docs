import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  @Input() toggle: boolean;
  @Output() toggleEvent = new EventEmitter<boolean>();
  public time: any;
  // public assets = environment.assets;
  constructor(
    private router: Router,
    // private session: SessionService
  ) {

  }

  ngOnInit(): void {
    this.time = new Date().getTime();
  }
  toggleMenu() {
    this.toggle = !this.toggle;
    this.toggleEvent.emit(this.toggle);
    localStorage.setItem('sidebar', JSON.stringify({ value: this.toggle }));
  }
  //   access(roles: string[]) {
  //     return roles.some(x => console.log(x));
  //     // return roles.some(x => x === this.session.get().myrole.roles.slug);
  //   }
  //   t_access(role: string) {
  //     return role === this.session.getTrans().role;
  //   }
  //   t_authority(authority: string) {
  //     return authority === this.session.getTrans().authority_name;
  //   }
}
