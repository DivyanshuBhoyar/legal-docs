import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { SessionService } from 'src/app/tokens/session.service';
// import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
// import { ResetComponent } from '../modals/reset/reset.component';
// import { Users, UsersC, UsersTrans, UsersTransC } from 'src/app/users/users';
// import { TokensService } from 'src/app/tokens/tokens.service';
// import { TransporterService } from './../../transporter/transporter.services';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  public breadcrumb: string = '';
  public time = new Date();
  // public modalRef: BsModalRef;
  public selectRole: number;
  public selectAuthority = 'Rate approval';
  showRoles: boolean = false;
  public roles: Array<any> = ['Rate approval', 'Transporter Invoices'];
  // public User : Users = new UsersC();
  // public User: UsersTrans = new UsersTransC();
  constructor(
    private actRoute: ActivatedRoute,
    private router: Router,
    // private session: SessionService,
    // private token: TokensService,
  ) { }

  ngOnInit(): void {
    // this.User = Object.assign({}, this.session.getTrans());
    // this.selectRole = this.User.myrole.id;
    // this.selectAuthority = this.User.authority_name;
    // if (this.User.authority === 2) {
    //   this.showRoles = false;
    // } else {
    //   this.showRoles = true;
    // }
    // this.selectRole = this.User.loginId;
    this.actRoute.data.subscribe((suc) => {
      this.breadcrumb = suc.currentState;
    });
    setInterval(() => {
      this.time = new Date();
    }, 1000);
  }
  // logout() {
  //   this.token.deleteToken().subscribe((res) => {});
  //   this.session.destroy();
  //   this.router.navigate(['/login']);
  // }
  // resetPass() {
  //   this.modalRef = this.modalService.show(ResetComponent, {
  //     backdrop: true,
  //     ignoreBackdropClick: true,
  //     class: 'modal-lg',
  //   });
  //   this.modalRef.content.closeBtnName = 'Close';
  // }
  // roleSwitch() {
  //   this.User.authority_name = this.selectAuthority;

  //   this.session.setTrans(this.User);

  //   // tslint:disable-next-line:no-conditional-assignment
  //   if (
  //     this.selectAuthority === 'Transporter Invoices' &&
  //     this.User.role === 'centerplanning'
  //   ) {
  //     this.router.navigate(['/transporter', 'transporterInvoice', 'approval1']);
  //   }
  //   // tslint:disable-next-line:no-conditional-assignment
  //   if (
  //     this.selectAuthority === 'Rate approval' &&
  //     this.User.role === 'centerplanning'
  //   ) {
  //     this.router.navigate(['/transporter', 'approval1']);
  //   }
  //   if (
  //     this.selectAuthority === 'Transporter Invoices' &&
  //     this.User.role === 'cphod'
  //   ) {
  //     this.router.navigate(['/transporter', 'transporterInvoice', 'approval3']);
  //   }
  //   if (
  //     this.selectAuthority === 'Rate approval' &&
  //     this.User.role === 'cphod'
  //   ) {
  //     this.router.navigate(['/transporter', 'approval3-list']);
  //   }
  // }
}

// let index = this.User.authorities.findIndex( x => x.id == this.selectRole);
//   if(index != -1)
//   {
//     this.User.myrole = this.User.authorities[index];
//     this.session.set(this.User);
//     this.router.navigate(['/dashboard','overview', new Date().getTime()]);
//   }
