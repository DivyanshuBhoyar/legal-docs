import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OtpComponent } from './otp/otp.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AuthComponent } from './auth.component';
import { SpinnerComponent } from '../shared-components/spinner/spinner.component';


@NgModule({
  declarations: [
    AuthComponent,
    OtpComponent,
    RegistrationComponent,
    LoginComponent,
    SpinnerComponent
  ],
  imports: [CommonModule, AuthRoutingModule, FormsModule]
})
export class AuthModule { }
