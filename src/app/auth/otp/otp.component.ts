import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss'],
})
export class OtpComponent implements OnInit {
  buttonSpin: boolean = false;
  otp: string = '';
  sentEmailID: string = this.Auth.loginUser.email;
  otpError = () => (this.otp.length < 6)

  constructor(private Auth: AuthService, private toastr: ToastrService, private router: Router) { }

  onSubmit() {
    if (!this.otpError()) {
      this.Auth.loginUser.otp = this.otp
      this.Auth.loginWithEmailOTP().subscribe(res => {
        this.buttonSpin = false
        this.otp = ""
        if (res.success) {
          this.toastr.success("Login Successful", "Noice")
          this.router.navigate(['/tester'])
        }
      })
    }
  }

  onClick() {
    this.buttonSpin = true
  }

  ngOnInit(): void { }
}
