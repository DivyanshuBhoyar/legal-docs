import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr'
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import NewUser from 'src/app/models/NewUser';
import { AuthService } from 'src/app/services/auth.service';
import UserReg from '../../models/UserReg'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  newUser: NewUser = {
    firstname: '',
    lastname: '',
    email: '',
    mobile: '',
    department: 'DART',
    emp_code: '',
    role: 1,
  };

  buttonClicked: boolean = false;
  subscription: Subscription

  constructor(private Auth: AuthService, private toastr: ToastrService, private router: Router) {
  }

  ngOnInit(): void { }



  onSubmit(newuser: NewUser) {
    console.log(this.newUser);
    this.Auth.registerUser(this.newUser).subscribe(response => {
      this.buttonClicked = true;
      if (response.message.includes("User Added Successfully")) {
        this.toastr.success("Registration Successful", 'Welcome, you can now log in')
        this.newUser = { firstname: '', lastname: '', email: '', mobile: '', department: 'DART', emp_code: '', role: 1, }
      }
      this.router.navigate(['/auth/login'])
    });
  }
  onclick() {
    this.buttonClicked = !this.buttonClicked;
    console.log(this.buttonClicked);
  }
}
