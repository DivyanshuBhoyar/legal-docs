import { Component, NgModule, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  buttonSpin: boolean = false;
  email: string = ''
  constructor(private Auth: AuthService, private router: Router, private toastr: ToastrService) { }

  onSubmit() {
    if (!this.validateEmailField()) {
      this.Auth.loginUser.email = this.email

      this.Auth.checkEmailExists({ 'email': this.email }).subscribe(res => {
        this.buttonSpin = false

        this.email = ''
        if (res.message.includes("Mail has been sent ")) {
          setTimeout(() => {
            this.router.navigate(['auth/otp-validate'])
          }, 2000)
          this.toastr.success("Please check your email", "OTP sent")
        }
      });
    }
  }

  validateEmailField() {
    const valid = /^\S+@\S+$/g
    return !valid.test(this.email)
  }

  onClick() {
    if (!this.validateEmailField()) {
      this.buttonSpin = true
    }
  }

  ngOnInit(): void { }
}
